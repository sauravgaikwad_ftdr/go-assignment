package main

import (
	"context"
	"encoding/json"
	//"os"
	"fmt"
	"log"
	"net/http"
	"strconv"
//	"reflect"
	//"time"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)



func ConnectDB()  {

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	//:removed
	var err  error
	client, err = mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	//collection := client.Database("demo").Collection("EmployeeCollection")

	//return collection
}


type ErrorResponse struct {
	StatusCode   int    `json:"status"`
	ErrorMessage string `json:"message"`
}


func GetError(err error, w http.ResponseWriter) {

	log.Fatal(err.Error())
	var response = ErrorResponse{
		ErrorMessage: err.Error(),
		StatusCode:   http.StatusInternalServerError,
	}

	message, _ := json.Marshal(response)

	w.WriteHeader(response.StatusCode)
	w.Write(message)
}


//---------------------------------------------------------------------------------------------------------------




var client *mongo.Client


type Fields struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Id   int             `json:"id,omitempty" bson:"id,omitempty"`
	UserId  int             `json:"userId" bson:"userId,omitempty"`
	Designation string            `json:"designation" bson:"designation,omitempty"`
}

type Fields1 struct{
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Id   int             `json:"id,omitempty" bson:"id,omitempty"`

	Firstname string	`json:"Firstname" bson:"Firstname,omitempty"`
	Lastname string		`json:"Lastname" bson:"Lastname,omitempty"`
	Email string		`json:"Email" bson:"Email,omitempty"`

}

type Mix struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Id   int             `json:"id,omitempty" bson:"id,omitempty"`
	UserId  int             `json:"userId" bson:"userId,omitempty"`
	Designation string            `json:"designation" bson:"designation,omitempty"`
	Firstname string	`json:"Firstname" bson:"Firstname,omitempty"`
	Lastname string		`json:"Lastname" bson:"Lastname,omitempty"`
	Email string		`json:"Email" bson:"Email,omitempty"`
}


func updateEmployeeDetails(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)


	id := params["id"]

	var result Fields1

	collection := client.Database("demo").Collection("UserCollection")

	// Create filter
	//filter := bson.M{"_id": id}

	a,err := strconv.ParseInt(id,10,64)
	filter := bson.M{"id": a }

	

	_ = json.NewDecoder(r.Body).Decode(&result)
	
	//err = collection.FindOne(context.TODO(), filter).Decode(&result)

	
	


	update := bson.D{
		{"$set", bson.D{
			{"Email", result.Email},
			
		}},
	}

	err1 := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&result)

	if err1 != nil {
		GetError(err, w)
		return
	}

	//result.Id = id

	json.NewEncoder(w).Encode(result)
}



func deleteEmployee(w http.ResponseWriter, r *http.Request) {
	
	w.Header().Set("Content-Type", "application/json")

	
	var params = mux.Vars(r)

	id:=params["id"]

	
	
	collection := client.Database("demo").Collection("EmployeeCollection")
	//collection := ConnectDB()
	
	a,err := strconv.ParseInt(id,10,64)
	filter := bson.M{"id": a }

	//filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}







func createEmployee(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var result Mix
	var  r1 Fields
	var  r2 Fields1

	
	_ = json.NewDecoder(r.Body).Decode(&result)



	var id int =result.Id
	var uid int = result.UserId
	var des string = result.Designation
	var fname string= result.Firstname
	var lname string = result.Lastname
	var em string = result.Email

	r1.Id=id
	r2.Id=id
	r1.UserId=uid
	r1.Designation=des
	r2.Firstname=fname
	r2.Lastname=lname
	r2.Email=em


	
	collection := client.Database("demo").Collection("EmployeeCollection")
	collection1 := client.Database("demo").Collection("UserCollection")
	
	res, err := collection.InsertOne(context.TODO(), r1)
	res1, err1 := collection1.InsertOne(context.TODO(), r2)

	if err != nil {
		GetError(err, w)
		return
	}

	if err1 != nil {
		GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(res)
	json.NewEncoder(w).Encode(res1)
}


func getEmployee(w http.ResponseWriter, r *http.Request) {
	
	w.Header().Set("Content-Type", "application/json")

	var result Fields
	var result1 Fields1
	
	var params = mux.Vars(r)

	
	id:=params["id"]
	//if errr != nil{
	//	fmt.Println("Error in Parsing")
//	}
    //fmt.Printf("%t",id)
	collection := client.Database("demo").Collection("EmployeeCollection")
	collection1 := client.Database("demo").Collection("UserCollection")
	fmt.Println(id)

	//collection := ConnectDB()

	
	a,err := strconv.ParseInt(id,10,64)
	filter := bson.M{"id": a }
	filter1 := bson.M{"id": a }


	
	err = collection.FindOne(context.TODO(), filter).Decode(&result)
	err1 := collection1.FindOne(context.TODO(), filter1).Decode(&result1)

	//fmt.Println(result)

	if err != nil {
		GetError(err, w)
		fmt.Println("Error in data 1")
		return
	}
	if err1 != nil {
		GetError(err1, w)
		fmt.Println("Error in data 2")
		return
	}

	//fmt.Printf("hiiiiiii %t",result)

	json.NewEncoder(w).Encode(result)
	json.NewEncoder(w).Encode(result1)
}

func GetEmployeeDetails(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	collection := client.Database("demo").Collection("EmployeeCollection")

	var result []Fields

	//collection := ConnectDB()

	
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		GetError(err, w)
		return
	}

	
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		
		var res Fields
		
		err := cur.Decode(&res) 
		if err != nil {
			log.Fatal(err)
		}

		
		result = append(result, res)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(result)

	

}


func main() {
	fmt.Println("Starting the application...")

	ConnectDB()
	
	router := mux.NewRouter()
	router.HandleFunc("/person", GetEmployeeDetails).Methods("GET")
	router.HandleFunc("/person/{id}", deleteEmployee).Methods("DELETE")
	router.HandleFunc("/person", createEmployee).Methods("POST")
	router.HandleFunc("/person/{id}", getEmployee).Methods("GET")
	router.HandleFunc("/person/{id}", updateEmployeeDetails).Methods("PUT")
	http.ListenAndServe(":7000", router)
}